package com.globant.dev.practices.problems;

import com.globant.dev.practices.problems.sameisland.SameIsland;
import com.globant.dev.practices.problems.sameisland.Point;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SameIslandTest {

    private SameIsland sameIsland;

    /*
     * Example Map:
     * |1|1|1|1|0|0|
     * |1|1|1|1|0|0|
     * |1|0|0|0|0|0|
     * |0|0|1|1|1|1|
     * |1|0|0|0|0|1|
     * |1|1|0|0|0|0|
     */
    private int[][] baseMap(){
        return new int[][] {
                {1,1,1,1,0,0},
                {1,1,1,1,0,0},
                {1,0,0,0,0,0},
                {0,0,1,1,1,1},
                {1,0,0,0,0,1},
                {1,1,0,0,0,0},
        };
    }

    @Test
    void testSameIslandSameRow(){
        Point start = new Point(0,0);
        Point end = new Point(0,3);
        sameIsland = new SameIsland(baseMap(),start,end);
        Assertions.assertTrue(sameIsland.isSameIsland(),"Must be the same island");
    }

    @Test
    void testSameIslandSameColumn(){
        Point start = new Point(0,0);
        Point end = new Point(2,0);
        sameIsland = new SameIsland(baseMap(),start,end);
        Assertions.assertTrue(sameIsland.isSameIsland(),"Must be the same island");
    }

    @Test
    void testNotSameIsland(){
        Point start = new Point(1,1);
        Point end = new Point(5,5);
        sameIsland = new SameIsland(baseMap(),start,end);
        Assertions.assertFalse(sameIsland.isSameIsland(),"Must be the same island");
    }

    @Test
    void testSameIsland(){
        Point start = new Point(0,3);
        Point end = new Point(2,0);
        sameIsland = new SameIsland(baseMap(),start,end);
        Assertions.assertTrue(sameIsland.isSameIsland(),"Must be the same island");
    }
}