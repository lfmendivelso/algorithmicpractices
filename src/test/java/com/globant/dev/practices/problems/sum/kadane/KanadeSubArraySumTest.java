package com.globant.dev.practices.problems.sum.kadane;

import lombok.extern.java.Log;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log
class KanadeSubArraySumTest {

    private int[] basicCase = {-2,3,2,-1};
    private int[] complexCase = {-1, 3, -5, 4, 6, -1, 2, -7, 13, -3};



    @Test
    void testKanadeMaxSum() {
        int result = KanadeSubArraySum.kanadeMaxSum(basicCase);
        Assertions.assertEquals(5, result);
    }

    @Test
    void testKanadeMaxSum2() {
        int result = KanadeSubArraySum.kanadeMaxSum(complexCase);
        Assertions.assertEquals(17, result);
    }

    @Test
    void testKanadeMaxSubArray() {
        List<Integer> result = KanadeSubArraySum.kanadeMaxSubArraySum(basicCase);
        List<Integer> expected = Arrays.asList(3,2);
        Assertions.assertEquals(expected, result);
    }

    @Test
    void testKanadeMaxSubArray2() {
        List<Integer> result = KanadeSubArraySum.kanadeMaxSubArraySum(complexCase);
        List<Integer> expected = Arrays.asList(4,6,-1,2,-7,13);
        Assertions.assertEquals(expected, result);
    }

    @Test
    void testMultipleCasesBasic(){
        for(Map.Entry<String,KanadeTestCase> kanadeCaseEntry : generateCases().entrySet()){
            log.info(kanadeCaseEntry.getKey());
            KanadeTestCase kanadeCase = kanadeCaseEntry.getValue();
            int sum = KanadeSubArraySum.kanadeMaxSum(kanadeCase.input);
            Assertions.assertEquals(kanadeCase.expectResult,sum);
        }
    }

    @Test
    void testMultipleCases(){
        for(Map.Entry<String,KanadeTestCase> kanadeCaseEntry : generateCases().entrySet()){
            log.info(kanadeCaseEntry.getKey());
            KanadeTestCase kanadeCase = kanadeCaseEntry.getValue();
            List<Integer> result = KanadeSubArraySum.kanadeMaxSubArraySum(kanadeCase.input);
            List<Integer> expected = Arrays.stream(kanadeCase.expectSubArray).boxed().collect(Collectors.toList());
            Assertions.assertEquals(expected,result);
        }
    }

    Map<String, KanadeTestCase> generateCases(){
        Map<String,KanadeTestCase> testCases = new LinkedHashMap<>();
        testCases.put("Test1",new KanadeTestCase(new int[]{5, 7, 12, 0, -8, 6},new int[]{5, 7, 12,0},24));
        testCases.put("Test2",new KanadeTestCase(new int[]{6, -2, -3, 4, -1, 10 },new int[]{6, -2, -3, 4, -1, 10 },14));
        testCases.put("Test3",new KanadeTestCase(new int[]{-1, -3, 4, -7, 0, 2},new int[]{4},4));
        testCases.put("Test4",new KanadeTestCase(new int[]{1, -5, 2, -1, 3} ,new int[]{2, -1, 3}  ,4));
        return testCases;
    }


    class KanadeTestCase{
        int[] input;
        int[] expectSubArray;
        int expectResult;

        public KanadeTestCase(int[] input, int[] expectSubArray, int expectResult) {
            this.input = input;
            this.expectSubArray = expectSubArray;
            this.expectResult = expectResult;
        }
    }
}

