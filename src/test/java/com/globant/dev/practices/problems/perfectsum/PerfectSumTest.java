package com.globant.dev.practices.problems.perfectsum;


import com.globant.dev.practices.problems.sum.PerfectSum;
import lombok.extern.java.Log;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

@Log
class PerfectSumTest {

    PerfectSum perfectSum;

    @Test
    void testFinSums1() {
        int[] values = {2,3,4,5};
        int expectedSum = 10;
        perfectSum = new PerfectSum(values,expectedSum);
        perfectSum.finSums();
        perfectSum.getSubSequences().forEach(x -> log.info(x));
        Assertions.assertEquals(1,perfectSum.getSubSequences().size());
    }

    @Test
    void testFinSums2() {
        int[] values = {5, 10, 12, 13, 15, 18};
        int expectedSum = 30;
        perfectSum = new PerfectSum(values,expectedSum);
        perfectSum.finSums();
        perfectSum.getSubSequences().forEach(x -> log.info(x));
        Assertions.assertEquals(3,perfectSum.getSubSequences().size());
    }

    @Test
    void testFinSums3() {
        int[] values = {1, 2, 3, 4};
        int expectedSum = 5;
        perfectSum = new PerfectSum(values,expectedSum);
        perfectSum.finSums();
        perfectSum.getSubSequences().forEach(x -> log.info(x));
        Assertions.assertEquals(2,perfectSum.getSubSequences().size());
    }
}