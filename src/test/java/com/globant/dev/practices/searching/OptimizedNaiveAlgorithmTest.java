package com.globant.dev.practices.searching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class OptimizedNaiveAlgorithmTest {

    OptimizedNaiveAlgorithm naiveAlgorithm;


    @Test()
    void testFindPattern_scenarie_Null() {
        naiveAlgorithm= new OptimizedNaiveAlgorithm(null,null);
        Assertions.assertThrows(IllegalArgumentException.class, ()->{naiveAlgorithm.findPattern();});
    }

    @Test
    void testFindPattern_scenarie_Blank() {
        naiveAlgorithm= new OptimizedNaiveAlgorithm("","");
        Assertions.assertThrows(IllegalArgumentException.class, ()->{naiveAlgorithm.findPattern();});
    }

    @Test
    void testFindPattern_scenarie_OneLetterNoMatch() {
        naiveAlgorithm= new OptimizedNaiveAlgorithm("a","b");
        List<Integer> result = naiveAlgorithm.findPattern();
        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    void testFindPattern_scenarie_OneLetterMatch() {
        naiveAlgorithm= new OptimizedNaiveAlgorithm("a","a");
        List<Integer> result = naiveAlgorithm.findPattern();
        Assertions.assertEquals(1,result.size());
    }

    @Test
    void testFindPattern_scenarie_base() {
        naiveAlgorithm= new OptimizedNaiveAlgorithm("AABAACAADAABAABA","AABA");
        List<Integer> result = naiveAlgorithm.findPattern();
        Assertions.assertEquals(2, result.size());
    }

    @Test
    void testFindPattern_scenarie_base2() {
        naiveAlgorithm= new OptimizedNaiveAlgorithm("ABCEABCDABCEABCD","ABCD");
        List<Integer> result = naiveAlgorithm.findPattern();
        Assertions.assertEquals(2, result.size());
    }
}