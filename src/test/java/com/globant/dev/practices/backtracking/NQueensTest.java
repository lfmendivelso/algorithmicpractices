package com.globant.dev.practices.backtracking;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class NQueensTest {

    private static final String MUST_HAVE = "Must have solution";
    private static final String MUST_NO_HAVE = "Must no have solution";
    
    NQueens solution;

    @Test
    public void test0Queen(){
        solution = new NQueens(0);
        Assertions.assertTrue(solution.findSolution(),MUST_HAVE);
        solution.printSolution();
    }

    @Test
    public void test1Queen(){
        solution = new NQueens(1);
        Assertions.assertTrue(solution.findSolution(),MUST_HAVE);
        solution.printSolution();
    }

    @Test
    public void test2Queen(){
        solution = new NQueens(2);
        Assertions.assertFalse(solution.findSolution(),MUST_NO_HAVE);
        solution.printSolution();
    }

    @Test
    public void test3Queen(){
        solution = new NQueens(3);
        Assertions.assertFalse(solution.findSolution(),MUST_NO_HAVE);
        solution.printSolution();
    }

    @Test
    public void test4Queen(){
        solution = new NQueens(4);
        Assertions.assertTrue(solution.findSolution(),MUST_HAVE);
        solution.printSolution();
    }

    @Test
    public void test9Queen(){
        solution = new NQueens(9);
        Assertions.assertTrue(solution.findSolution(),MUST_NO_HAVE);
        solution.printSolution();
    }

    @Test
    public void test12Queen(){
        solution = new NQueens(12);
        Assertions.assertTrue(solution.findSolution(),MUST_HAVE);
        solution.printSolution();
    }

}