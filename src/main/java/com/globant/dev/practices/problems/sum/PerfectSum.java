package com.globant.dev.practices.problems.sum;

import java.util.*;

public class PerfectSum {

    private int[] values;
    private int expectedSum;
    private Set<String> subSequences;

    public PerfectSum(int[] values, int expectedSum) {
        this.values = values;
        this.expectedSum = expectedSum;
        subSequences = new HashSet<>();
    }

    public void finSums() {
        for (int i = 0; i < values.length; i++) {
            String operation = "";
            sum(0, i, operation);
        }
    }

    private void sum(int sum, int post, String operation) {
        String op = operation + (operation.isEmpty() ? "" : ",") + values[post];
        sum += values[post];
        if (sum == expectedSum) {
            String result = "{"+op+"}";
            if(!subSequences.contains(result)) subSequences.add(result);
        } else {
            for (int j = post; j < values.length; j++) {
                if (j + 1 < values.length) sum(sum, j + 1, op);
            }
        }
    }

    public Set<String> getSubSequences() {
        return subSequences;
    }
}
