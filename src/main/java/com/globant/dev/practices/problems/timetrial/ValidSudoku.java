package com.globant.dev.practices.problems.timetrial;

import lombok.extern.java.Log;
import java.util.HashSet;
import java.util.Set;

/**
 * Problem: https://www.programcreek.com/2014/05/leetcode-valid-sudoku-java/
 */
@Log
public class ValidSudoku {

    public static final char DOT = '.';
    public static final int LIMIT = 9;

    /**
     * Validation rules:
     * 1.) Must be 9x9
     * 2.) A number can be repeat in the same row.
     * 3.) A number can be repeat in the same column.
     * 4.) A number can be repeat in the same box 3x3.
     *
     * @param sudoku is the matrix of characters to validated
     * @return true if it is valid / false if it is not valid.
     */
    public static boolean isValidSudoku(char[][] sudoku) {
        boolean isValid = isValidStructure(sudoku);
        if (isValid) {
            // Rows and Column validation
            for (int i = 0; i < LIMIT && isValid; i++) {
                Set<Character> valuesCol = new HashSet<>();
                Set<Character> valuesInRow = new HashSet<>();
                for (int j = 0; j < LIMIT && isValid; j++) {
                    if(sudoku[i][j] != DOT ){
                        if (!valuesCol.contains(sudoku[i][j]) && Character.isDigit(sudoku[i][j])){
                            valuesCol.add(sudoku[i][j]);
                        } else {
                            log.info("By Row");
                            isValid = Boolean.FALSE;
                        }
                    }
                    if(sudoku[j][i] != DOT){
                        if (!valuesInRow.contains(sudoku[j][i]) && Character.isDigit(sudoku[j][i])) {
                            valuesInRow.add(sudoku[j][i]);
                        } else {
                            log.info("By Column");
                            isValid = Boolean.FALSE;
                        }
                    }
                }
            }
            if(isValid) isValid = validateRegions(sudoku);
        }
        return isValid;
    }

    private static boolean validateRegions(char[][] sudoku){
        boolean isValid = Boolean.TRUE;

        for(int globalRow=0;globalRow<LIMIT && isValid;globalRow=globalRow+3){
            for(int globalCol=0;globalCol<LIMIT && isValid;globalCol=globalCol+3){
                Set<Character> regionSet = new HashSet<>();
                for(int r=globalRow;r<globalRow+3 && isValid;r++){
                    for(int c=globalCol;c<globalCol+3 && isValid;c++){
                        if(sudoku[r][c] != DOT){
                            if(!regionSet.contains(sudoku[r][c])){
                                regionSet.add(sudoku[r][c]);
                            }
                            else{
                                log.info("By Region");
                                isValid = Boolean.FALSE;
                            }
                        }
                    }
                }
            }
        }
        return isValid;
    }

    private static boolean isValidStructure(char[][] sudoku) {
        boolean valid = Boolean.TRUE;
        if (sudoku == null) {
            valid = Boolean.FALSE;
        } else {
            if (sudoku.length != LIMIT) {
                valid = Boolean.FALSE;
            } else {
                for (int i = 0; i < LIMIT && valid; i++) {
                    if (sudoku[i].length != LIMIT) valid = Boolean.FALSE;
                }
            }
        }
        return valid;
    }

    public static void main(String[] agrs) {
        char[][] sudoku = {
                {'5', '3', '.', '.', '7', '.', '.', '.', '.'},
                {'6', '.', '.', '1', '9', '5', '.', '.', '.'},
                {'.', '9', '8', '.', '.', '.', '.', '6', '.'},
                {'8', '.', '.', '.', '6', '.', '.', '.', '3'},
                {'4', '.', '.', '8', '.', '3', '.', '.', '1'},
                {'7', '.', '.', '.', '2', '.', '.', '.', '6'},
                {'.', '6', '.', '.', '.', '.', '2', '8', '.'},
                {'.', '.', '.', '4', '1', '9', '.', '.', '5'},
                {'.', '.', '.', '.', '8', '.', '.', '7', '9'}
        };
        log.info("This sudoku is valid? R/ " + isValidSudoku(sudoku));

        char[][] sudoku2 = {
                {'5', '3', '.', '.', '7', '.', '.', '.', '.'},
                {'6', '.', '.', '1', '9', '5', '.', '.', '.'},
                {'.', '9', '8', '.', '.', '.', '.', '6', '.'},
                {'8', '.', '.', '.', '6', '.', '.', '.', '3'},
                {'4', '.', '.', '8', '.', '3', '.', '.', '1'},
                {'7', '.', '.', '.', '2', '.', '.', '.', '6'},
                {'.', '6', '.', '.', '.', '.', '2', '8', '.'},
                {'.', '.', '.', '4', '1', '9', '.', '.', '5'},
                {'.', '.', '.', '.', '8', '.', '9', '7', '9'}
        };
        log.info("This sudoku2 is valid? R/ " + isValidSudoku(sudoku2));

        char[][] sudoku3 = {
                {'5', '3', '.', '.', '7', '.', '.', '.', '.'},
                {'6', '.', '.', '1', '9', '5', '.', '.', '.'},
                {'.', '9', '8', '.', '.', '.', '.', '6', '1'},
                {'8', '.', '.', '.', '6', '.', '.', '.', '3'},
                {'4', '.', '.', '8', '.', '3', '.', '.', '1'},
                {'7', '.', '.', '.', '2', '.', '.', '.', '6'},
                {'.', '6', '.', '.', '.', '.', '2', '8', '.'},
                {'.', '.', '.', '4', '1', '9', '.', '.', '5'},
                {'.', '.', '.', '.', '8', '.', '.', '7', '9'}
        };
        log.info("This sudoku3 is valid? R/ " + isValidSudoku(sudoku3));

        char[][] sudoku4 = {
                {'.', '3', '5', '.', '7', '.', '.', '.', '.'},
                {'6', '5', '.', '1', '9', '.', '.', '.', '.'},
                {'5', '9', '8', '.', '.', '.', '.', '6', '.'},
                {'8', '.', '.', '.', '6', '.', '.', '.', '3'},
                {'4', '.', '.', '8', '.', '3', '.', '.', '1'},
                {'7', '.', '.', '.', '2', '.', '.', '.', '6'},
                {'.', '6', '.', '.', '.', '.', '2', '8', '.'},
                {'.', '.', '.', '4', '1', '9', '.', '.', '5'},
                {'.', '.', '.', '.', '8', '.', '.', '7', '9'}
        };
        log.info("This sudoku4 is valid? R/ " + isValidSudoku(sudoku4));
    }
}
