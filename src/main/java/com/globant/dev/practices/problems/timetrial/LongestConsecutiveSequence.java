package com.globant.dev.practices.problems.timetrial;

import lombok.extern.java.Log;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Log
public class LongestConsecutiveSequence {

    public static int longestConsecutiveSequence(int[] array){
        Set<Integer> solution = Arrays.stream(array).boxed().collect(Collectors.toSet());
        int max = Integer.MIN_VALUE;
        for(int value : array){
            int current = 1;
            int carry = 1;
            // Forward
            while(solution.contains(value+carry)){
                carry++;
                current++;
            }
            //Backward
            carry=1;
            while (solution.contains(value-carry)){
                carry++;
                current++;
            }
            max = Math.max(max,current);
        }
        return max;
    }

    public static Set<Integer> longestConsecutiveSequenceArray(int[] array){
        Set<Integer> values = Arrays.stream(array).boxed().collect(Collectors.toSet());
        Set<Integer> solution = new HashSet<>();
        int max = Integer.MIN_VALUE;
        for(int value : array){
            int current = 1;
            int carry = 1;
            Set<Integer> tempSolution = new HashSet<>();
            tempSolution.add(value);
            // Forward
            while(values.contains(value+carry)){
                tempSolution.add(value+carry);
                carry++;
                current++;
            }
            //Backward
            carry=1;
            while (values.contains(value-carry)){
                tempSolution.add(value-carry);
                carry++;
                current++;
            }
            if(current > max){
                max=current;
                solution = tempSolution;
            }
        }
        return solution;
    }

    public static void main(String[] agrs){
        int[] array = {100, 4, 200, 1, 3, 2};
        log.info("The Longest Consecutive Sequence has a length of: "+longestConsecutiveSequence(array));
        log.info("The Longest Consecutive Sequence has a length of: "+longestConsecutiveSequenceArray(array).toString());
    }

}
