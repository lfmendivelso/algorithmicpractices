package com.globant.dev.practices.problems.sameisland;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Class: SameIsland
 *
 * Problem:
 * The problem to be solved has the following elements:
 * 1.) You receive as input a map made up of zeros and ones, which represents 0 = water and 1 = earth.
 * 2.) A starting point that refers to an X1 and Y2 coordinate within the map.
 * 3.) An end point that refers to coordinate X2 and Y2.
 *
 * And it is necessary to know if it is possible to walk from the starting point to the end point,
 * therefore it is found that both points are on the same island.
 *
 * Example Map:
 * |1|1|1|1|0|0|
 * |1|1|1|1|0|0|
 * |1|0|0|0|0|0|
 * |0|0|1|1|1|1|
 * |1|0|0|0|0|1|
 * |1|1|0|0|0|0|
 *
 * Input:
 * Start Point: (0,3) - End Point: (2,0)
 * Start Point: (0,3) - End Point: (5,0)
 * 
 * Output:
 * True
 * False
 */
public class SameIslandBasic {

    int[][] map;
    Point start;
    Point end;
    int nX;
    int nY;
    Set<Point> evaluated;

    public SameIslandBasic(int[][] map, Point start, Point end) {
        this.map = map;
        nX = map.length;
        nY = map[0].length;
        this.start = start;
        this.end = end;
        evaluated = new HashSet<>();
    }

    public Boolean isSameIsland() {
        return solve(start, Boolean.FALSE);
    }

    private Boolean solve(Point point, boolean result) {
        List<Point> adjacentPoints = getAdjacentPoints(point);
        for (int i = 0; i < adjacentPoints.size() && !result; i++) {
            Point p = adjacentPoints.get(i);
            int value = map[p.x][p.y];
            evaluated.add(p);
            if (value == NumberUtils.INTEGER_ONE) {
                if (end.equals(p)) {
                    result = Boolean.TRUE;
                } else {
                    result = solve(p, result);
                }
            }
        }
        return result;
    }

    private List<Point> getAdjacentPoints(Point point) {
        List<Point> adjacentPoints = Arrays.asList(
                new Point(point.x, point.y + 1),
                new Point(point.x, point.y - 1),
                new Point(point.x + 1, point.y),
                new Point(point.x - 1, point.y)

        );

        adjacentPoints = adjacentPoints.stream()
                .filter(p -> (p.x >= 0 && p.x <= nX - 1 && p.y >= 0 && p.y <= nY - 1 && !evaluated.contains(p)))
                .collect(Collectors.toList());

        return adjacentPoints;
    }

}
