package com.globant.dev.practices.problems.sum.kadane;


import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class KanadeSubArraySum {

    public static int kanadeMaxSum(int[] array) {
        int max = 0;
        int currentMax = 0;
        for (int i = 0; i < array.length; i++) {
            currentMax = Math.max(array[i], currentMax + array[i]);
            max = Math.max(max, currentMax);
        }
        return max;
    }

    public static List<Integer> kanadeMaxSubArraySum(int[] array) {
        List<Integer> solution = new ArrayList<>();
        int max = Integer.MIN_VALUE;
        int current = 0;
        for (int i = 0; i < array.length; i++) {
            int sum = current + array[i];
            current = Math.max(sum,array[i]);
            if(current == sum){
                solution.add(array[i]);
            }
            else{
                if(sum < array[i]) solution.clear();
                solution.add(array[i]);
                current = array[i];
            }
            max = Math.max(max,current);
        }
        current = max;
        for(int back = solution.size()-1;back>=0;back--){
            current = current - solution.get(back);
            if(current > max){
                max = max - solution.get(back);
                solution = solution.subList(0,back);
            }
        }
        return solution;
    }

}
