package com.globant.dev.practices.fit.leaderboard;

import lombok.extern.java.Log;

import java.util.*;

@Log
public class LeaderBoard {

    Map<String,Player> players;

    public LeaderBoard(){
        players = new HashMap<>();
    }

    public void addScore(String playerId, int score){
        Player player = players.get(playerId);
        player.setScore(score);
        players.put(playerId,player);
    }

    public void reset(String playerId){
        Player player = players.get(playerId);
        player.setScore(0);
        players.put(playerId,player);
    }

    public List<Player> top(int k){
        List<Player> playersList = new ArrayList<>(players.values());
        Comparator<Player> comparator = Comparator.comparing(Player::getScore);
        playersList.sort(comparator.reversed());
        return playersList.subList(0,k);
    }

    public static void main(String[] agrs){
        LeaderBoard leaderBoard = new LeaderBoard();

        Player p1 = new Player("1","P1",100);
        Player p2 = new Player("2","P2",87);
        Player p3 = new Player("3","P3",20);
        Player p4 = new Player("4","P4",35);
        Player p5 = new Player("5","P5",50);
        leaderBoard.players.put("1",p1);
        leaderBoard.players.put("2",p2);
        leaderBoard.players.put("3",p3);
        leaderBoard.players.put("4",p4);
        leaderBoard.players.put("5",p5);

        List<Player> top = Arrays.asList(p1,p2,p5);
        List<Player> result = leaderBoard.top(3);
        log.info("-------------------------");
        log.info("Resultado: "+result.toString());
        log.info("Correct? R/ "+top.equals(result));

        leaderBoard.reset(p1.getId());
        top = Arrays.asList(p2,p5,p4);
        result = leaderBoard.top(3);
        log.info("-------------------------");
        log.info("Resultado: "+result.toString());
        log.info("Correct? R/ "+top.equals(result));

        leaderBoard.addScore(p1.getId(),86);
        top = Arrays.asList(p2,p1,p5);
        result = leaderBoard.top(3);
        log.info("-------------------------");
        log.info("Resultado: "+result.toString());
        log.info("Correct? R/ "+top.equals(result));
    }
}
