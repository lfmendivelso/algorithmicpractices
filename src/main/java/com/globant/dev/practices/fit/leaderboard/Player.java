package com.globant.dev.practices.fit.leaderboard;

import lombok.Data;

@Data
public class Player {

    private String id;
    private String name;
    private Integer score;

    public Player(String id, String name, Integer score) {
        this.id = id;
        this.name = name;
        this.score = score;
    }
}
