package com.globant.dev.practices.fit;

import lombok.extern.java.Log;

@Log
public class KsumConsecutiveSubArray {

    public static int sum(int[] arr, int k){
        int sum = 0;
        int counter = 0;
        for(int i=0; i< arr.length ; i++){
            sum+= arr[i];
            if(sum == k){
                counter++;
                sum = arr[i];
            }
            else if(sum >k){
                sum = arr[i];
            }

        }
        return counter;
    }

    public static void main(String[] agrs){
        int k = 4;
        int[] arr = {2,2,5,4,3,1,5,6,2,3,4,6,8,9,1,1,2,3};
        log.info("Total consecutive subarray with sum equal to" +k+ " :"+sum(arr,k));
    }
}
