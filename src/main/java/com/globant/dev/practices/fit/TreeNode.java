package com.globant.dev.practices.fit;

import lombok.extern.java.Log;

@Log
public class TreeNode {

    String alias;
    TreeNode left;
    TreeNode right;
    TreeNode next;

    public TreeNode(String alias, TreeNode left, TreeNode right) {
        this.left = left;
        this.right = right;
        this.alias = alias;
    }

    public static void setNextPointer(TreeNode root){
        if(root != null){
            if(root.left != null && root.right !=null){
                root.left.next = root.right;
                if( root.left.right != null && root.right.left != null){
                    root.left.right.next = root.right.left;
                }
            }
            setNextPointer(root.left);
            setNextPointer(root.right);
        }
    }

    private static String nodeStr(TreeNode root){
        String str = "";
        if(root != null){
            if(root.left != null){
                if(root.left.alias != null) str += "'"+root.left.alias+"'";
                if(root.left.next != null) str += " -> ";
            }
            if(root.right != null){
                if(root.right.alias != null) str += "'"+root.right.alias+"'";
                if(root.right.next != null) str += " -> ";
                if(root.right.next == null) str+="\n";
            }
            str += nodeStr(root.left)+nodeStr(root.right);
        }
        return str;
    }

    public static void printTree(TreeNode root){
        log.info("\n'"+root.alias+"'\n"+nodeStr(root));
    }

    public static void main(String[] agrs){
        TreeNode Q = new TreeNode("Q",null,null);
        TreeNode W = new TreeNode("W",null,null);
        TreeNode A = new TreeNode("A", Q,W);

        TreeNode E = new TreeNode("E",null,null);
        TreeNode R = new TreeNode("R",null,null);
        TreeNode B = new TreeNode("B",E,R);

        TreeNode X = new TreeNode("X",A,B);

        setNextPointer(X);
        printTree(X);
    }

}
