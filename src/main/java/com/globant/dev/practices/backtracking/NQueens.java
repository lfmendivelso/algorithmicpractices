package com.globant.dev.practices.backtracking;

import lombok.extern.java.Log;
import org.apache.commons.lang3.math.NumberUtils;

@Log
public class NQueens {

    private Integer n;
    private int[][] board;

    NQueens(Integer n){
        this.n = n;
        board = new int[n][n];
    }

    Boolean findSolution(){
        return solve(board,NumberUtils.INTEGER_ZERO);
    }

    private Boolean solve(int[][] currentBoard, Integer column){
        if(column >= n){
            return Boolean.TRUE;
        }
        for(int row = 0; row < n ; row++){
            if(isValidPosition(currentBoard,row,column)){
                board[row][column] = NumberUtils.INTEGER_ONE;
                if(solve(currentBoard,column+1).equals(Boolean.TRUE)){
                    return Boolean.TRUE;
                }
                currentBoard[row][column]=0;
            }
        }
        return Boolean.FALSE;
    }

    private boolean isValidPosition(int[][] currentBoard, Integer row, Integer column){
        boolean valid = Boolean.TRUE;
        Integer tRow;
        Integer tCol;
        for(tCol=0;tCol<column && valid;tCol++){
            if(currentBoard[row][tCol]==NumberUtils.INTEGER_ONE) valid=Boolean.FALSE;
        }
        for(tRow=row,tCol=column;tRow>=0 && tCol>=0 && valid;tRow--,tCol--){
            if(currentBoard[tRow][tCol] == NumberUtils.INTEGER_ONE) valid=Boolean.FALSE;
        }

        for(tRow=row,tCol=column;tRow<n && tCol>=0 && valid;tRow++,tCol--){
            if(currentBoard[tRow][tCol] == NumberUtils.INTEGER_ONE) valid=Boolean.FALSE;
        }
        return valid;
    }


    public void printSolution(){
        StringBuilder boardModel = new StringBuilder("\n");
        Integer queen = 1;
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                if(board[i][j] > 0){
                    boardModel.append("Q").append(queen).append("|");
                    queen++;
                }
                else{
                    boardModel.append("__|");
                }
            }
            boardModel.append("\n");
        }
        log.info(boardModel.toString());
    }


    public static void main(String[] agrs) {
        Integer numberOfQueens = 4;
        NQueens solution = new NQueens(numberOfQueens);
        boolean hasSolution = solution.findSolution();
        if(hasSolution){
            solution.printSolution();
        }
        else{
            log.info("The scenario doesn't have solution.");
        }
    }


}