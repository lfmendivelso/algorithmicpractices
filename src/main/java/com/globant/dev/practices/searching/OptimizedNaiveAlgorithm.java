package com.globant.dev.practices.searching;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import java.util.LinkedList;
import java.util.List;

@Log
public class OptimizedNaiveAlgorithm {

    String text;
    String pattern;

    public OptimizedNaiveAlgorithm(String text, String pattern) {
        this.text = text;
        this.pattern = pattern;
    }

    public List<Integer> findPattern() {
        List<Integer> indexesMatches = new LinkedList<>();
        if (StringUtils.isNotBlank(text) && StringUtils.isNotBlank(pattern)) {
            int indexText = 0;
            int limit = (text.length() - pattern.length());
            while (indexText <= limit) {
                int patternIndex;
                boolean found = true;
                for (patternIndex = 0; patternIndex < pattern.length() && found; patternIndex++) {
                    if (text.charAt(indexText + patternIndex) != pattern.charAt(patternIndex)) found = false;
                }
                if (found) {
                    indexesMatches.add(indexText);
                    log.info("Pattern found in the index :" + indexText);
                    indexText = indexText + patternIndex;
                } else if (patternIndex == NumberUtils.INTEGER_ZERO) {
                    indexText++;
                } else {
                    indexText = indexText + patternIndex;
                }
            }
            return indexesMatches;
        } else {
            throw new IllegalArgumentException("The text and pattern can be null, empty or black");
        }

    }
}
