package com.globant.dev.practices.searching;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import java.util.LinkedList;
import java.util.List;

@Log
public class NaiveAlgorithm {

    private String text;
    private String pattern;

    public NaiveAlgorithm(String text, String pattern){
        this.text=text;
        this.pattern=pattern;
    }

    public List<Integer> findPattern(){
        List<Integer> indexesWithThePattern = new LinkedList<>();
        if(StringUtils.isNotBlank(text) && StringUtils.isNotBlank(pattern)){
            for(int textIndex=0; textIndex <= (text.length() - pattern.length());textIndex++){
                int patternIndex;
                boolean found=true;
                for(patternIndex=0; patternIndex < pattern.length() && found; patternIndex++){
                    if(text.charAt(textIndex+patternIndex) != pattern.charAt(patternIndex)){
                        found=false;
                    }
                }
                if(found){
                    indexesWithThePattern.add(textIndex);
                    log.info("Pattern found in the index :"+textIndex);
                }
            }
        }
        else{
            throw new IllegalArgumentException("The text and pattern can be empty");
        }
        return indexesWithThePattern;
    }

}

